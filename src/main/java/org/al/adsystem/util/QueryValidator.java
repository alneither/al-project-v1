package org.al.adsystem.util;

public class QueryValidator {
    private static final String SEARCH_QUERY_PATTERN = "[\\w|\\s|\\W]{2,}|^\\S";

    public static boolean isQueryValid(final String query) {
        return query.matches(SEARCH_QUERY_PATTERN);
    }
}
