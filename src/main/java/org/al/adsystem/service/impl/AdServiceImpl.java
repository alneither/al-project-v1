package org.al.adsystem.service.impl;

import org.al.adsystem.dao.iface.AdProviderDAO;
import org.al.adsystem.dao.iface.SearchProviderDAO;
import org.al.adsystem.model.domain.bean.Advert;
import org.al.adsystem.service.iface.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AdServiceImpl implements AdService {
    @Autowired
    private AdProviderDAO dao;
    @Autowired
    private SearchProviderDAO searchDao;

    @Override
    @Transactional
    public List<Advert> getCurrentAds(final int count) {
        return dao.getCurrentAds(count);
    }

    @Override
    @Transactional
    public List<Advert> getUserAds(final int id, final int count) {
        return dao.getUserAds(id, count);
    }

    @Override
    @Transactional
    public void addNewAdvert(final Advert newAd) {
        dao.addNewAdvert(newAd);
    }

    @Override
    @Transactional
    public List<Advert> getSearchResults(final String query) {
        return searchDao.searchAds(query);
    }

    public void setDao(final AdProviderDAO dao) {
        this.dao = dao;
    }

    public void setSearchDao(final SearchProviderDAO searchDao) {
        this.searchDao = searchDao;
    }
}
