package org.al.adsystem.dao.iface;

import org.al.adsystem.model.domain.bean.Advert;
import java.util.List;

public interface SearchProviderDAO {

    List<Advert> searchAds(String query);
}
