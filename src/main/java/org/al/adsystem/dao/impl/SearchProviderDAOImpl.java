package org.al.adsystem.dao.impl;

import org.al.adsystem.dao.iface.SearchProviderDAO;
import org.al.adsystem.model.domain.bean.Advert;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.lucene.search.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.al.adsystem.util.Constant.*;

@Repository
public class SearchProviderDAOImpl implements SearchProviderDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Advert> searchAds(String userQuery) {
        Session session = sessionFactory.getCurrentSession();
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(Advert.class).get();

        Query query = qb
                .keyword()
                .onFields(HEADER_FIELD, BODY_FIELD, CONTACT_FIELD)
                .matching(userQuery)
                .createQuery();

        javax.persistence.Query persQuery =
                fullTextSession.createFullTextQuery(query, Advert.class);
        List<Advert> results = persQuery.getResultList();
        return Collections.unmodifiableList(results);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
