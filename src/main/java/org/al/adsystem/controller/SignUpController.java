package org.al.adsystem.controller;

import org.al.adsystem.service.iface.UserService;
import org.al.adsystem.util.UserDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static org.al.adsystem.util.Constant.*;

@Controller
public class SignUpController {
    private static final String PASSWORD_REPEAT = "password-repeat";
    @Autowired
    private UserService service;

    @RequestMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping(value = "/add-user", method = RequestMethod.POST)
    public String signUpNewUser(final Model model, @RequestParam final String login,
                                @RequestParam final String password,
                                @RequestParam(PASSWORD_REPEAT) final String passwordRepeat) {
        if (UserDataValidator.validateUserData(login, password, passwordRepeat)) {
            service.signUpUser(login, password);
            model.addAttribute(INFORMATION_MESSAGE_ATTR_NAME, SUCCESSFUL_REGISTRATION_MESSAGE);
            return "index";
        } else {
            model.addAttribute(ERROR_MESSAGE_ATTR_NAME, INCORRECT_DATA_ENTERED_MESSAGE);
            return "registration";
        }
    }

    public void setService(final UserService service) {
        this.service = service;
    }

}
