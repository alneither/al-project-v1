package org.al.adsystem.controller;

import org.al.adsystem.model.domain.bean.Advert;
import org.al.adsystem.service.iface.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;

import static org.al.adsystem.util.Constant.*;

@Controller
public class MainPageFeedController {
    @Autowired
    private AdService service;
    private static final int ADS_PER_PAGE_INDEX = 5;

    @RequestMapping(value = {"/", "/index"})
    public String mainPageWithContent(Model model) {
        List<Advert> adverts = service.getCurrentAds(ADS_PER_PAGE_INDEX);
        model.addAttribute(CURRENT_ADS_LIST_NAME, adverts);
        return "index";
    }

    @RequestMapping("/search-result")
    public String searchResultPage() {
        return "index";
    }

    public void setService(AdService service) {
        this.service = service;
    }
}
